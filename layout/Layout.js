import React from "react";
import NavBar from "../components/NavBar";

import CategoryNavBar from "../components/CategoryNavBar";
import { useState } from "react";
import { useContext } from "react";
import { CategoryContext } from "../context/CategoryContext";
import ContainerHeader from "../components/ContainerHeader";
import { CartContext } from "../context/CartConext";
import  Footer from "./../components/Footer"
function Layout({ children }) {
  const [category, setCategory] = useState("⚡ New In");
  const [cart,setCart ] = useState([]);


  return (
    <div className="p-5 mx-2 md:mx-10">
      <CartContext.Provider value={{cart,setCart}}>
      <CategoryContext.Provider value={{ category, setCategory }}>
        <NavBar />
        <CategoryNavBar />
        <ContainerHeader/>
        {children}
        <Footer/>
      </CategoryContext.Provider>
      </CartContext.Provider>
    </div>
  );
}

export default Layout;
