import { Provider } from 'react-redux'
import '../styles/globals.css'
import store from '../redux/store'
import NavBar from '../components/NavBar'
import CategoryNavBar from '../components/CategoryNavBar'
import categoryContext from '../context/CategoryContext' 
import Layout from '../layout/Layout'
function MyApp({ Component, pageProps }) {
  return (
  <Layout>
    <Component {...pageProps} />
  </Layout>)
}

export default MyApp
