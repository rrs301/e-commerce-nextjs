import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import Products from '../../shared/Products';
import Gallary from '../../components/ProductItem/Gallary';
import ProductConfig from '../../components/ProductItem/ProductConfig';
import ProductDetail from '../../components/ProductItem/ProductDetail';
import SimilarProductList from '../../components/ProductItem/SimilarProductList';

function ProductDetails() {
    const router=useRouter();
    const [productDetail,setProductDetail]=useState([]);
    const itemId=router.query.itemId;
     console.log(itemId);
     
    useEffect(()=>{
     
        getProductDetail();
    },[itemId])
    const getProductDetail=()=>{
        const productDetail_=Products.productList.filter
        (item=>item.id==itemId);
        setProductDetail(productDetail_)
    }
  return (
    <div>
       {productDetail? 
       <div className='grid grid-cols-1  mt-7 gap-5 sm:grid-cols-2 sm:px-10'>
          <Gallary productDetail={productDetail[0]}/>
          <ProductConfig productDetail={productDetail[0]}/>
        </div>:null}
        <ProductDetail productDetail={productDetail[0]}/>
        <SimilarProductList productList={Products.productList}/>
    </div>
  )
}


export default ProductDetails