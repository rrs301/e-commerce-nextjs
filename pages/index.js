
import styles from '../styles/Home.module.css'

import HomeContainer from '../components/Home/HomeContainer'
export default function Home() {
  return (
    <div className={styles.container}>

    <div className='flex '>
    {/* <CategoryNavBar/> */}
    <div className='flex flex-col w-full'>
   
      <HomeContainer/>
    </div>
      
    </div>
  
   
    </div>
  )
}
