import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import ProductsList from './../../../shared/Products'
import CategoryNavBar from '../../../components/CategoryNavBar';
import ContainerHeader from '../../../components/ContainerHeader';
import ProductContainer from '../../../components/ProductList/ProductListContainer';
function Products() {

    const router=useRouter();
    const product=router.query.ProductName;
    const [productList,setProductList]=useState([])
    useEffect(()=>{
  const data=product!='new'?
  ProductsList.productList.filter(item=>item.category==product):
   product.productList;
   setProductList(data)
   console.log(data)
    },[product])
  return (
    <div>
      <div className='flex mt-5'>
      {productList.length>0?
        <ProductContainer product={product} 
        productList={productList} />:
        <h1 className='text-[17px] text-gray-400 text-center w-full'>No Products Available 🥺</h1>}
    </div>
    </div>
   
  )
}




  
export default Products