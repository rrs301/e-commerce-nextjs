import { createSlice } from "@reduxjs/toolkit";

export const genderSlice=createSlice({
    name:'gender',
    initialState:{
        value:0
    },

    reducers:{
        setSelectedGender:(state,action)=>{
            console.log(action.payload.value)
            state.value=action.payload.value
        }
    } 
})
export const { setSelectedGender} = genderSlice.actions
export default genderSlice.reducer