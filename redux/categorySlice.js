import { createSlice } from "@reduxjs/toolkit"

export const categorySlice = createSlice({
    name: 'category',
    initialState: {
      id: 1,
      name: '⚡ New In'
    },
    reducers: {
      setSelectedCategory:(state,action)=>{
          state.id=action.payload.id
          state.name=action.payload.name
      },
     
    }
  })
  
  // Action creators are generated for each case reducer function
  export const { setSelectedCategory} = categorySlice.actions
  
  export default categorySlice.reducer