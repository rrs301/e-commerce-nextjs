/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['res.cloudinary.com','static.nike.com','img.freepik.com'],
  },
}

module.exports = nextConfig
