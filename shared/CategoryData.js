const categoryData=[
    {
        id:1,
        name:'⚡ New In',
        value:'new'
    },
    {
        id:2,
        name:'👕 Clothing',
        value:'clothing'
    },
    {
        id:3,
        name:'👞 Shoes',
        value:'shoes'
    },
    {
        id:4,
        name:'👓 Accessories',
        value:'accessories'
    },
    {
        id:5,
        name:'📱 Mobile Cases',
        value:'mobilecase'
    },
    {
        id:6,
        name:'🎁 Gift',
        value:'gift'
    }
]

export default {
    categoryData
}