const productList=[
    {
        id:1,
        name:'Nike Air VaporMax 2023 Flyknit',
        type:'Mens Shoes',
        price:210,
        category:'shoes',
        desc:'Have you ever walked on Air? Step into the Air VaporMax 2023 to see how its done. The innovative tech is revealed through the perforated sockliner (pull it out to see more). The stretchy Flyknit upper is made with at least 20% recycled content by weight.',
        size:[7,7.5,8,8.5,9,9.5,10],
        posterImage:'https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/009b026e-33bc-44ef-a733-7df87195cb05/react-infinity-run-3-mens-road-running-shoes-1bRq75.png',
        images:[
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363308/air_max_270_mens_shoes_Kk_Lc_GR_b3549966bc.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363316/air_max_270_mens_shoes_Kk_Lc_GR_1319d1ecc4.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363329/air_max_270_mens_shoes_Kk_Lc_GR_fe486b3b6a.jpg',
            },

        ]
    },
    {
        id:2,
        name:'Nike Air VaporMax 2023 Flyknit',
        type:'Mens Shoes',
        price:210,
        category:'shoes',

        desc:'Have you ever walked on Air? Step into the Air VaporMax 2023 to see how its done. The innovative tech is revealed through the perforated sockliner (pull it out to see more). The stretchy Flyknit upper is made with at least 20% recycled content by weight.',
        size:[7,7.5,8,8.5,9,9.5,10],
        posterImage:'https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/aab6a404-44ad-4b55-a502-5fa1961ccf17/nikecourt-zoom-vapor-cage-4-rafa-mens-hard-court-tennis-shoes-dMxMqL.png',
        images:[
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363308/air_max_270_mens_shoes_Kk_Lc_GR_b3549966bc.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363316/air_max_270_mens_shoes_Kk_Lc_GR_1319d1ecc4.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363329/air_max_270_mens_shoes_Kk_Lc_GR_fe486b3b6a.jpg',
            },

        ]
    },
    {
        id:3,
        name:'Nike Air VaporMax 2023 Flyknit',
        type:'Mens Shoes',
        price:210,
        category:'shoes',

        desc:'Have you ever walked on Air? Step into the Air VaporMax 2023 to see how its done. The innovative tech is revealed through the perforated sockliner (pull it out to see more). The stretchy Flyknit upper is made with at least 20% recycled content by weight.',
        size:[7,7.5,8,8.5,9,9.5,10],
        posterImage:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363308/air_max_270_mens_shoes_Kk_Lc_GR_b3549966bc.jpg',
        images:[
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363308/air_max_270_mens_shoes_Kk_Lc_GR_b3549966bc.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363316/air_max_270_mens_shoes_Kk_Lc_GR_1319d1ecc4.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363329/air_max_270_mens_shoes_Kk_Lc_GR_fe486b3b6a.jpg',
            },

        ]
    },
    {
        id:4,
        name:'Nike Air VaporMax 2023 Flyknit',
        type:'Mens Shoes',
        desc:'Have you ever walked on Air? Step into the Air VaporMax 2023 to see how its done. The innovative tech is revealed through the perforated sockliner (pull it out to see more). The stretchy Flyknit upper is made with at least 20% recycled content by weight.',
        size:[7,7.5,8,8.5,9,9.5,10],
        price:210,
        category:'shoes',

        posterImage:'https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/3285cd0f-b4eb-409d-b62a-f44513bb78c9/quest-5-mens-road-running-shoes-1tmPXN.png',
        images:[
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363308/air_max_270_mens_shoes_Kk_Lc_GR_b3549966bc.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363316/air_max_270_mens_shoes_Kk_Lc_GR_1319d1ecc4.jpg',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363329/air_max_270_mens_shoes_Kk_Lc_GR_fe486b3b6a.jpg',
            },

        ]
    },
    {
        id:5,
        name:'Nike Dri-FIT Legend',
        type:'Mens',
        desc:'Have you ever walked on Air? Step into the Air VaporMax 2023 to see how its done. The innovative tech is revealed through the perforated sockliner (pull it out to see more). The stretchy Flyknit upper is made with at least 20% recycled content by weight.',
        size:['S','M','L'],
        price:45,
        category:'clothing',

        posterImage:'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/19e24d78-2ddb-4094-bfc6-ec86f5576ae7/dri-fit-legend-mens-long-sleeve-fitness-top-StC1Ng.png',
        images:[
            {
                url:'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/19e24d78-2ddb-4094-bfc6-ec86f5576ae7/dri-fit-legend-mens-long-sleeve-fitness-top-StC1Ng.png',
            },
            {
                url:'https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/5d50123c-3048-4455-bbda-18ef1002f0be/dri-fit-legend-mens-long-sleeve-fitness-top-StC1Ng.png',
            },
            {
                url:'https://res.cloudinary.com/dknvsbuyy/image/upload/v1686363329/air_max_270_mens_shoes_Kk_Lc_GR_fe486b3b6a.jpg',
            },

        ]
    }
]


export default{
    productList
}