import Image from "next/image";
import React, { useContext, useEffect, useState } from "react";
import logo from "./../public/images/logo.png";
import { HiOutlineBars3CenterLeft, HiOutlineHeart, HiOutlineShoppingCart } from "react-icons/hi2";
import { CartContext } from "../context/CartConext";
import { useRouter } from "next/router";
import SideNav from "./NavBar/SideNav";
const USER_IMAGE =
  "https://res.cloudinary.com/dknvsbuyy/image/upload/v1686314044/1617826370281_30f9a2a96a.jpg";
function NavBar() {
  const router=useRouter();
  const {cart,setCart}=useContext(CartContext);
  const [toggle,setToggle]=useState(false)
  // console.log(cart)
  return (
    <div
      className="flex   items-center 
     justify-between"
    >
      <HiOutlineBars3CenterLeft onClick={()=>setToggle(!toggle)}
       className="  md:hidden  text-[24px] mr-2"/>
       {toggle?<div className="absolute top-20 w-[70%]">
       <SideNav setToggle={(val)=>setToggle(val)}/>
       </div>:null}
       
      <Image src={logo} className="w-[150px] cursor-pointer" alt="logo"
       onClick={()=>router.push('/')} />
      <input
        type="text"
        placeholder="Search"
        className="bg-slate-200 p-2 rounded-md 
         w-[90%] outline-none hidden md:block mx-3 md:mx-10"
      />
      <div className="flex gap-4 items-center">
        <div className="flex gap-2">
          <HiOutlineShoppingCart className="text-[25px]" />
          <h3>{cart?.length}</h3>
        </div>
        <HiOutlineHeart className="text-[22px]"/>

        <Image
          src={USER_IMAGE}
          alt="user-image"
          width={40}
          height={40}
          className="rounded-full"
        />
      </div>
    </div>
  );
}

export default NavBar;
