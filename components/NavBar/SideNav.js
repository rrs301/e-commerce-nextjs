import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react'
import { CategoryContext } from '../../context/CategoryContext';
import CategoryData from '../../shared/CategoryData';

function SideNav({setToggle}) {
    const [activeIndex,setAciveIndex]=useState(0)
    // const dispatch = useDispatch()
     const router = useRouter();
     const {category,setCategory}=useContext(CategoryContext);
     
     useEffect(()=>{
     
       
     },[])
     const onCategoryClick=(item,index)=>{
       setAciveIndex(index);
       setCategory(item.name)
        setToggle(false)
      if(item.value=='new')
      {
       router.push("/");
       return ;
 
      }
       router.push("/products/"+item.value)
     }
   return (
     <div className='p-5 bg-white h-[100vh]'>
         <div className='mt-4 flex flex-col gap-10
          '>
         {CategoryData.categoryData.map((item,index)=>(
         <div key={item.id}>
            <h1 className={`text-[23px] mb-5 hover:scale-110
            hover:font-medium transition-all duration-200 
            cursor-pointer ${index==activeIndex?'font-medium':null}`}
            onClick={()=>onCategoryClick(item,index)}>{item.name}</h1>
         </div>
       ))}
         </div>
       
     </div>
   )
 }
export default SideNav