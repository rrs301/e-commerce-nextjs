import React, { useContext } from "react";
import products from "./../../shared/Products";
import { HiOutlineBars3BottomRight, HiOutlineShoppingCart } from "react-icons/hi2";
import { CartContext } from "../../context/CartConext";
import storage from "../../services/storage";
import { useRouter } from "next/router";
import Product from "./Product";
function ProductContainer({product,productList}) {
  const {cart,setCart}=useContext(CartContext);
  const router=useRouter();

  const onProductClick=(id)=>{
    // storage.setCart(item)
    // setCart((prevState)=>[
    //   ...prevState,item
    // ]);

    // router.push('/productItem/'+id);
  }
  return (
    <div className="grid grid-cols-2 gap-2 
    md:grid-cols-3 md:gap-3 lg:grid-cols-4 lg:gap-5">
      {productList.map((item) => (
        <div key={item.id} onClick={()=>onProductClick(item.id)}>
          <Product item={item} />
        </div>
      ))}
    </div>
  );
}



export default ProductContainer;
