import React, { useContext, useState } from 'react'
import categoryData from './../shared/CategoryData'
import { setSelectedCategory } from '../redux/categorySlice';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { CategoryContext } from '../context/CategoryContext';

function CategoryNavBar() {
    const [activeIndex,setAciveIndex]=useState(0)
   // const dispatch = useDispatch()
    const router = useRouter();
    const {category,setCategory}=useContext(CategoryContext);
    
    useEffect(()=>{
    
      
    },[])
    const onCategoryClick=(item,index)=>{
      setAciveIndex(index);
      setCategory(item.name)
   
     if(item.value=='new')
     {
      router.push("/");
      return ;

     }
      router.push("/products/"+item.value)
    }
  return (
    <div className='p-5  hidden md:block '>
        {/* <h1 className='font-bold text-[24px]'>Explore</h1> */}
        <div className='mt-4 flex gap-9 justify-center'>
        {categoryData.categoryData.map((item,index)=>(
        <div key={item.id}>
           <h1 className={`text-[17px] mb-5 hover:scale-110
           hover:font-medium transition-all duration-200 
           cursor-pointer ${index==activeIndex?'font-medium':null}`}
           onClick={()=>onCategoryClick(item,index)}>{item.name}</h1>
        </div>
      ))}
        </div>
      
    </div>
  )
}

export default CategoryNavBar