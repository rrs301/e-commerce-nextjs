import Image from 'next/image'
import React, { useEffect, useState } from 'react'

function Gallary({productDetail}) {
    console.log(productDetail)
    const [posterImage,setPosterImage]=useState()
    useEffect(()=>{
        setPosterImage(productDetail?.posterImage);
    },[])
  return (
    <div>
   <Image src={posterImage?posterImage:productDetail?.posterImage} 
         height={140} width={400}  alt='image' 
         className='object-contain rounded-md h-[400px]' />
         <div className='flex gap-4 mt-5'>
            {productDetail?.images.map((item,index)=>(
               
                    <Image key={index} src={item.url}
                    width={100} height={100} alt='image'
                    onClick={()=>setPosterImage(item.url)}
                     className='object-cover rounded-md cursor-pointer'/>
                
            ))}
         </div>
    </div>
  )
}

export default Gallary