import React from 'react'

function ProductDetail({productDetail}) {
  return (
    <div className='mt-5'>
        <h2 className='font-bold text-[22px]'>Product Details</h2>
        <h3>{productDetail?.desc}</h3>
    </div>
  )
}

export default ProductDetail