import React from 'react'
import Product from '../ProductList/Product'

function SimilarProductList({productList}) {
    console.log(productList)
  return (
    <div className='mt-5'> 
        <h2 className='font-bold text-[20px]'>You Might Also Like</h2>
        <div className='flex gap-2 md:gap-7 overflow-x-auto '>
            {productList.map((item,index)=>index<=3&&(
                
                    <Product key={index} item={item}/>
             
            ))}
        </div>
    </div>
  )
}

export default SimilarProductList