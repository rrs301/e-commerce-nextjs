import React from 'react'

function ProductConfig({productDetail}) {

  return (
    <div className=''>
        <h1 className='text-[25px] font-bold'>{productDetail?.name}</h1>
        <h3 className='text-[20px]'>{productDetail?.type}</h3>
        <h3 className='text-[25px] text-blue-600 mt-5 font-bold'>${productDetail?.price}</h3>

       {productDetail?.size.length?
       <div className='mt-8'>
       <h3 className='font-bold'>Select an option</h3>
       <select className="select w-full max-w-xs mt-2 bg-slate-100">
  <option disabled defaultValue>Select Your Size</option>
  {productDetail.size.map((item,index)=>(
        <option key={index}>{item}</option>
  ))}
 
</select>
       </div>
       :null}
       <div className='flex flex-col justify-start md:w-[60%] lg:w-[40%]'>
       <button className='mt-5 bg-black p-4 px-10 text-white rounded-full'>Add to Cart</button>
       <button className='mt-5 b p-4 px-10 border-[2px] text-black rounded-full'>Favorite</button>
       </div>
       <div className='mt-5'>
        <h2 className='font-bold text-[17px]'>Free Shipping*</h2>
<h2>Arrives by Fri, Jun 9 to28117</h2>
       </div>
    </div>
  )
}

export default ProductConfig