import React from "react";
import BannerOffer from "../../shared/BannerOffer";
import Image from "next/image";
import { useState } from "react";
import { useRouter } from "next/router";

function HomeContainer() {
  const [banneroffer, setBannerOffer] = useState(BannerOffer.BannerOffer);
  const router=useRouter();
  return (
    <div className=" w-full  grid md:grid-cols-4 gap-4 mt-5">
      <div className="col-span-2" onClick={()=>router.push('/products/shoes')}>
        <Image
          src={banneroffer[0].image}
          alt="banner"
          width="1000"
          placeholder="blur"
          blurDataURL="https://res.cloudinary.com/dknvsbuyy/image/upload/v1686328556/static_skeleton_screen_1580ff0e0f.png"
          className="w-full cursor-pointer 
          rounded-md
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
        <Image
          src={banneroffer[3].image}
          alt="banner"
          width="1000"
          className="w-full mt-5 cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
      </div>
        <Image
          src={banneroffer[1].image}
          alt="banner"
          width="1000"
          className="w-full cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
        <Image
          src={banneroffer[2].image}
          alt="banner"
          width="1000"
          className="w-full cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
     <Image
          src={banneroffer[1].image}
          alt="banner"
          width="1000"
          className="w-full cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
        <Image
          src={banneroffer[2].image}
          alt="banner"
          width="1000"
          className="w-full cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
   <div className="col-span-2 " onClick={()=>router.push('/products/shoes')}>
        <Image
          src={banneroffer[0].image}
          alt="banner"
          width="1000"
          className="w-full cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
        <Image
          src={banneroffer[3].image}
          alt="banner"
          width="1000"
          className="w-full mt-5 cursor-pointer 
          hover:scale-105 transition-all ease-in-out duration-150"
          height={500}
        />
      </div>
      
   
    </div>
  );
}

export default HomeContainer;
