import React, { useContext, useState } from 'react'
import { HiOutlineBars3BottomRight } from 'react-icons/hi2'
import { useDispatch, useSelector } from 'react-redux'
import { setSelectedGender } from '../redux/genderSlice';
import { CategoryContext } from '../context/CategoryContext';

function ContainerHeader() {
  //const category = useSelector(state => state.category.name)
  //const dispatch=useDispatch();
 // const storedGender=useSelector(state => state.gender?.value)
  const {category,setCategory}=useContext(CategoryContext);
  const [gender,setGender]=useState(0)
  return (
    <div className=' flex flex-col sm:flex-row justify-between w-full h-min
    pt-5 md:pt-0 items-center'>
        <h1 className='text-[20px] md:text-[24px] font-bold'>{category}</h1>
        <div className='flex  gap-7 
        mt-5 sm:mt-0 items-center'>
            <div className="join">
  <button onClick={()=>{setGender(0);}} 
  className={`btn join-item ${gender==0?'bg-gray-200':null}`}>👩‍⚖️ Women</button>
  <button onClick={()=>{setGender(1);}} 
  className={`btn join-item ${gender==1?'bg-gray-200':null}`} >👨‍🔧 Men</button>
            </div>
        
          <h1 className='flex border-[2px] p-2 rounded-md'>
            <HiOutlineBars3BottomRight className='text-[20px] mr-5'
          /> <span className='hidden md:block'>Sort</span></h1> 
        </div>
    </div>
  )
}

export default ContainerHeader